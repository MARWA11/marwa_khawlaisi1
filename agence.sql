-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 11 Avril 2015 à 15:27
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `agence`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

CREATE TABLE IF NOT EXISTS `aeroport` (
  `code` varchar(3) NOT NULL,
  `nom` varchar(200) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `pays` varchar(2) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `aeroport`
--

INSERT INTO `aeroport` (`code`, `nom`, `ville`, `pays`) VALUES
('dje', 'djerba_zarzis', 'djerba', 'Fr'),
('hmt', 'djerba_zarzis', 'djerba', 'tu'),
('M', 'marseille Provence airpot', 'Marseille', 'Fr'),
('Mr', 'marseille Provence aireport', 'France', 'Fr'),
('Mrs', 'marseille Provence airpot', 'Marseille', 'Fr'),
('tun', 'djerba_zarzis', 'tunisie', 'Tu');

-- --------------------------------------------------------

--
-- Structure de la table `appareil`
--

CREATE TABLE IF NOT EXISTS `appareil` (
  `id_appareil` int(5) NOT NULL AUTO_INCREMENT,
  `marque` varchar(45) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id_appareil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE IF NOT EXISTS `compagnie` (
  `id_compagnie` int(4) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id_compagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

CREATE TABLE IF NOT EXISTS `vol` (
  `code` varchar(5) NOT NULL,
  `id_companie` int(4) NOT NULL,
  `date_depart` date NOT NULL,
  `date_Arrivé` date NOT NULL,
  `code_aeroport-dép` varchar(3) NOT NULL,
  `code_aeroport-arrivée` varchar(3) NOT NULL,
  `id_appareil` int(5) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
